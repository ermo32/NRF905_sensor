EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_AM_FM:RFM95W-868S2 U?
U 1 1 5BDF1A82
P 5900 3650
F 0 "U?" H 5900 4328 50  0000 C CNN
F 1 "RFM95W-868S2" H 5900 4237 50  0000 C CNN
F 2 "" H 2600 5300 50  0001 C CNN
F 3 "http://www.hoperf.com/upload/rf/RFM95_96_97_98W.pdf" H 2600 5300 50  0001 C CNN
	1    5900 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5BDF1BA2
P 7450 3350
F 0 "J?" H 7549 3326 50  0000 L CNN
F 1 "Conn_Coaxial" H 7549 3235 50  0000 L CNN
F 2 "" H 7450 3350 50  0001 C CNN
F 3 " ~" H 7450 3350 50  0001 C CNN
	1    7450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3350 7250 3350
$Comp
L power:GND #PWR?
U 1 1 5BDF1BF3
P 7450 3750
F 0 "#PWR?" H 7450 3500 50  0001 C CNN
F 1 "GND" H 7455 3577 50  0000 C CNN
F 2 "" H 7450 3750 50  0001 C CNN
F 3 "" H 7450 3750 50  0001 C CNN
	1    7450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3750 7450 3550
$EndSCHEMATC
