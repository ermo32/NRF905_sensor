EESchema Schematic File Version 4
LIBS:NRF905_sensor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7950 1950 7400 1950
Wire Wire Line
	5450 3650 5500 3650
Wire Wire Line
	5450 3650 5450 4950
Wire Wire Line
	5450 4950 7500 4950
Wire Wire Line
	7500 4950 7500 2450
Wire Wire Line
	7500 2450 7950 2450
Wire Wire Line
	7950 2550 7550 2550
Wire Wire Line
	7550 2550 7550 5000
Wire Wire Line
	7550 5000 5400 5000
Wire Wire Line
	5400 5000 5400 3450
Wire Wire Line
	5400 3450 5500 3450
Wire Wire Line
	9150 1950 9150 5400
Wire Wire Line
	9150 5400 5000 5400
Wire Wire Line
	5000 5400 5000 3550
Wire Wire Line
	5000 3550 5500 3550
Wire Wire Line
	7950 2750 7600 2750
Wire Wire Line
	7600 2750 7600 5050
Wire Wire Line
	7600 5050 5350 5050
Wire Wire Line
	5350 5050 5350 3150
Wire Wire Line
	7950 2950 7700 2950
Wire Wire Line
	7700 2950 7700 5150
Wire Wire Line
	7700 5150 5250 5150
Wire Wire Line
	5050 5350 5050 3750
Wire Wire Line
	5050 3750 5500 3750
Wire Wire Line
	7950 3050 7750 3050
Wire Wire Line
	7750 3050 7750 5200
Wire Wire Line
	7750 5200 5200 5200
Wire Wire Line
	5200 5200 5200 3950
Wire Wire Line
	5200 3950 5500 3950
Wire Wire Line
	7950 3150 7800 3150
Wire Wire Line
	7800 3150 7800 5250
Wire Wire Line
	7800 5250 5150 5250
Wire Wire Line
	5150 5250 5150 3850
Wire Wire Line
	5150 3850 5500 3850
Wire Wire Line
	7950 3250 7850 3250
Wire Wire Line
	7850 3250 7850 5300
Wire Wire Line
	7850 5300 5100 5300
Wire Wire Line
	5100 5300 5100 4050
Wire Wire Line
	5100 4050 5500 4050
Wire Wire Line
	7650 2250 7650 2150
Wire Wire Line
	7650 2150 7850 2150
Wire Wire Line
	7950 2250 7850 2250
Wire Wire Line
	7850 2250 7850 2150
Connection ~ 7850 2150
Wire Wire Line
	3000 1650 3300 1650
Wire Wire Line
	3300 1650 3300 2050
Wire Wire Line
	3300 2050 3000 2050
Wire Wire Line
	6800 1650 6800 1850
Wire Wire Line
	6800 1850 6700 1850
Connection ~ 3300 1650
Wire Wire Line
	3000 4000 3000 4300
Wire Wire Line
	2550 4300 3000 4300
Wire Wire Line
	3900 4300 3900 4000
Wire Wire Line
	3450 4150 3450 4300
Connection ~ 3450 4300
Wire Wire Line
	7400 1800 7400 1950
Connection ~ 7400 1950
Wire Wire Line
	2550 4150 2550 4300
Connection ~ 3000 4300
Wire Wire Line
	3450 3000 3450 3800
Wire Wire Line
	3600 3800 3450 3800
Connection ~ 3450 3800
Wire Wire Line
	2550 3000 2550 3800
Wire Wire Line
	2700 3800 2550 3800
Connection ~ 2550 3800
Wire Wire Line
	2600 2600 3450 2600
Wire Wire Line
	3450 2600 3450 2700
Wire Wire Line
	7850 2150 7950 2150
Wire Wire Line
	3300 1650 6800 1650
Wire Wire Line
	3450 4300 3900 4300
Wire Wire Line
	3450 4300 3450 4450
Wire Wire Line
	3000 4300 3450 4300
Wire Wire Line
	3450 3800 3450 3850
Wire Wire Line
	2550 3800 2550 3850
$Comp
L modex:NRF905_module U2
U 1 1 5BE0CC5E
P 8550 2600
F 0 "U2" H 8550 3537 60  0000 C CNN
F 1 "NRF905_module" H 8550 3431 60  0000 C CNN
F 2 "modex:NRF905_module" H 8200 2450 60  0001 C CNN
F 3 "" H 8200 2450 60  0001 C CNN
	1    8550 2600
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4148 D1
U 1 1 5BE0D057
P 2850 1650
F 0 "D1" H 2850 1434 50  0000 C CNN
F 1 "1N4148" H 2850 1525 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2850 1475 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/1N4148_1N4448.pdf" H 2850 1650 50  0001 C CNN
	1    2850 1650
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N4148 D2
U 1 1 5BE12C39
P 2850 2050
F 0 "D2" H 2850 1834 50  0000 C CNN
F 1 "1N4148" H 2850 1925 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2850 1875 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/1N4148_1N4448.pdf" H 2850 2050 50  0001 C CNN
	1    2850 2050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BE12C9D
P 3450 4450
F 0 "#PWR01" H 3450 4200 50  0001 C CNN
F 1 "GND" H 3455 4277 50  0000 C CNN
F 2 "" H 3450 4450 50  0001 C CNN
F 3 "" H 3450 4450 50  0001 C CNN
	1    3450 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5BE12CEC
P 6800 2250
F 0 "#PWR02" H 6800 2000 50  0001 C CNN
F 1 "GND" H 6805 2077 50  0000 C CNN
F 2 "" H 6800 2250 50  0001 C CNN
F 3 "" H 6800 2250 50  0001 C CNN
	1    6800 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5BE12D3B
P 7650 2250
F 0 "#PWR04" H 7650 2000 50  0001 C CNN
F 1 "GND" H 7655 2077 50  0000 C CNN
F 2 "" H 7650 2250 50  0001 C CNN
F 3 "" H 7650 2250 50  0001 C CNN
	1    7650 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BE26F48
P 2550 2850
F 0 "R1" H 2620 2896 50  0000 L CNN
F 1 "12k" H 2620 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2480 2850 50  0001 C CNN
F 3 "~" H 2550 2850 50  0001 C CNN
	1    2550 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5BE2709B
P 3450 2850
F 0 "R3" H 3520 2896 50  0000 L CNN
F 1 "12k" H 3520 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3380 2850 50  0001 C CNN
F 3 "~" H 3450 2850 50  0001 C CNN
	1    3450 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BE271EE
P 2550 4000
F 0 "R2" H 2620 4046 50  0000 L CNN
F 1 "10k" H 2620 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2480 4000 50  0001 C CNN
F 3 "~" H 2550 4000 50  0001 C CNN
	1    2550 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5BE272AA
P 3450 4000
F 0 "R4" H 3520 4046 50  0000 L CNN
F 1 "10k" H 3520 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3380 4000 50  0001 C CNN
F 3 "~" H 3450 4000 50  0001 C CNN
	1    3450 4000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5BE276C3
P 7400 1800
F 0 "#PWR03" H 7400 1650 50  0001 C CNN
F 1 "VCC" H 7417 1973 50  0000 C CNN
F 2 "" H 7400 1800 50  0001 C CNN
F 3 "" H 7400 1800 50  0001 C CNN
	1    7400 1800
	1    0    0    -1  
$EndComp
Text Notes 9650 4550 0    50   ~ 0
7 -> CE\n8 -> PWR\n9 -> TXE\n4 -> CD\n3 -> DR\n2 -> AM\n10 -> CSN\n12 -> SO\n11 -> SI\n13 -> SCK\n\n5 -> power detect 1 (mail set)\n6 -> power detect 2 (mail reset)
$Comp
L pspice:CAP C1
U 1 1 5BE224DE
P 9750 2250
F 0 "C1" H 9928 2296 50  0000 L CNN
F 1 "100 nF" H 9928 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9750 2250 50  0001 C CNN
F 3 "~" H 9750 2250 50  0001 C CNN
	1    9750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5BE226E8
P 9750 2600
F 0 "#PWR06" H 9750 2350 50  0001 C CNN
F 1 "GND" H 9755 2427 50  0000 C CNN
F 2 "" H 9750 2600 50  0001 C CNN
F 3 "" H 9750 2600 50  0001 C CNN
	1    9750 2600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5BE2270D
P 9750 1900
F 0 "#PWR05" H 9750 1750 50  0001 C CNN
F 1 "VCC" H 9767 2073 50  0000 C CNN
F 2 "" H 9750 1900 50  0001 C CNN
F 3 "" H 9750 1900 50  0001 C CNN
	1    9750 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 1900 9750 2000
Wire Wire Line
	9750 2500 9750 2600
Wire Wire Line
	7900 5350 7900 3350
Wire Wire Line
	7900 3350 7950 3350
Wire Wire Line
	7900 5350 5050 5350
Wire Wire Line
	5500 3150 5350 3150
Wire Wire Line
	5500 3050 5250 3050
Wire Wire Line
	5250 3050 5250 5150
Wire Wire Line
	7950 2850 7650 2850
Wire Wire Line
	7650 2850 7650 5100
Wire Wire Line
	7650 5100 5300 5100
Wire Wire Line
	5300 5100 5300 2950
Wire Wire Line
	5300 2950 5500 2950
Wire Wire Line
	2100 2050 2550 2050
Wire Wire Line
	2100 1650 2600 1650
Wire Wire Line
	2600 1650 2600 2600
Connection ~ 2600 1650
Wire Wire Line
	2600 1650 2700 1650
Wire Wire Line
	2550 2050 2550 2700
Connection ~ 2550 2050
Wire Wire Line
	2550 2050 2700 2050
Wire Wire Line
	3900 3350 5500 3350
Wire Wire Line
	3900 3350 3900 3600
Wire Wire Line
	3000 3250 5500 3250
Wire Wire Line
	3000 3250 3000 3600
$Comp
L Transistor_FET:MMBF170 Q2
U 1 1 5BF11C43
P 3800 3800
F 0 "Q2" H 4005 3846 50  0000 L CNN
F 1 "MMBF170" H 4005 3755 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4000 3725 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30104.pdf" H 3800 3800 50  0001 L CNN
	1    3800 3800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:MMBF170 Q1
U 1 1 5BF14A51
P 2900 3800
F 0 "Q1" H 3105 3846 50  0000 L CNN
F 1 "MMBF170" H 3105 3755 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3100 3725 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30104.pdf" H 2900 3800 50  0001 L CNN
	1    2900 3800
	1    0    0    -1  
$EndComp
$Comp
L modex:Arduino_mini_mega_328_3V3_8MHz U1
U 1 1 5BFEEB07
P 6100 2400
F 0 "U1" H 6100 3237 60  0000 C CNN
F 1 "Arduino_mini_mega_328_3V3_8MHz" H 6100 3131 60  0000 C CNN
F 2 "modex:Arduino_Pro_Mini_Mega_328_3V3_8MHz" H 6100 2150 60  0001 C CNN
F 3 "" H 6100 2150 60  0001 C CNN
	1    6100 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1950 7400 1950
Wire Wire Line
	6700 2050 6800 2050
Wire Wire Line
	6800 2050 6800 2250
$EndSCHEMATC
